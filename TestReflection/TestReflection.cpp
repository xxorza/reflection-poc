// TestReflection.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Car.h"


int main()
{
	Car car;

	car.setValue("name", std::string("unnamed car"));
	car.setValue("maxSpeed", 3000);

	const auto speed = car.getValue<int32_t>("maxSpeed");

    return 0;
}

