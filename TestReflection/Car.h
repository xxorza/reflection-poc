#pragma once

#include "stdafx.h"

#include "Object.h"

class Car :
	public Object
{
public:
	Car();
	virtual ~Car();

private:
	std::string  name;
	int32_t maxSpeed;
	float width;
};

