#pragma once

#include "stdafx.h"

#include "DataType.h"

class Object
{
public:
	Object() {}
	virtual ~Object() {}

	Object(Object const&) = delete;
	Object& operator=(Object const&) = delete;

	template<typename T>
	void setValue(const std::string &name, const T&value) {
		const Field &field = fields.at(name);
		assert(field.type == getType<T>());

		*reinterpret_cast<T*>(field.address) = value;
	}

	template <typename T>
	const T& getValue(const std::string &name)const {
		const Field &field = fields.at(name);
		assert(field.type == getType<T>());

		return *reinterpret_cast<T*>(field.address);
	}

protected:
	template<typename T>
	void registerField(const std::string &name, T& field) {
		Field f;
		f.name = name;
		f.address = reinterpret_cast<void*>(&field);
		f.type = getType<T>();

		fields.insert({ name, f });
	}

private:
	struct Field {
		std::string name;
		void* address;
		DataType type;
	};
	std::map<std::string, Field> fields;


};

