#pragma once

#include "stdafx.h"

enum class DataType {
	Int32, 
	String,
	Float
};

template <typename T>
static inline DataType getType();

template <> 
static inline DataType getType<int32_t>() { return DataType::Int32; }

template <>
static inline DataType getType<std::string>() { return DataType::String; }

template <>
static inline DataType getType<float>() { return DataType::Float; }